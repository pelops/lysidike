Lysidike publishes incoming mqtt messages to various internet services 
like email.

![Pelops Overview](img/Microservice Overview.png)

```Lysidike``` is part of the collection of mqtt based microservices [pelops](https://gitlab.com/pelops). An overview
on the microservice architecture and examples can be found at (http://gitlab.com/pelops/pelops).
